stringsAnswers = {
  /**
   * Reduces a string by removing letters that repeat more than amount times.
   *
   * Example: reduceString('aaaabbb', 1) should reduce to 'ab'
   * Example: reduceString('aaaabbb', 2) should reduce to 'aabb'
   *
   * @param {String} str - String that is to be reduced
   * @param {Number} amount - The maximum number of adjacent repeated letters in the result string.
   * @returns {String} A string with no more than amount number of repeated letters.
   */
  reduceString: function reduceString(str, amount) {
    let newStr = '';
    let sum = 1;
    // Loop over each item in string
    for (let i = 0; i < str.length; i += 1) {
      const value = str[i];
      const nextValue = str[i + 1];
      // Check character next sibling
      if (value === nextValue) {
        sum += 1;
      } else {
        // If sum is more then amount then set equal to amount
        if (sum > amount) {
          sum = amount;
        } else if (sum === 0) {
          sum = 1;
        }
        // Add characters to string
        for (let a = 0; a < sum; a += 1) {
          newStr += value;
        }
        sum = 1;
      }
    }

    return newStr;
  },

  /**
   * Reverses a string of text
   *
   * Example: reverseString('abc') should be 'bca'
   *
   * @param {String} str - a string of text to be reversed
   * @returns {String} The original string of text str reversed.
   */
  reverseString: function reverseString(str) {
    const splitStr = str.split('');
    const reverseArray = splitStr.reverse();
    const joinArray = reverseArray.join('');
    return joinArray;
  },
};
